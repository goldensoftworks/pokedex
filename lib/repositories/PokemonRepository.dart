import 'package:Pokedex/clients/PokemonClient.dart';
import 'package:Pokedex/models/Pokemon.dart';
import 'package:http/http.dart';

class PokemonRepository {
  final _client = PokemonClient(Client());

  /// Get requesting pokemon
  Future<List<Pokemon>> getPokemon() async {
    return _client.getPokemon();
  }
}
