//import 'dart:js';

import 'package:Pokedex/models/Pokemon.dart';
import 'package:Pokedex/repositories/PokemonRepository.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:Pokedex/blocs/PokemonBloc.dart';

class HomeScreen extends StatefulWidget {
  HomeScreen({Key key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  int _selectedIndex = 0;
  String _title = 'Pokemon';

//  final _pokemonRepository = PokemonRepository();

  static const TextStyle optionStyle =
  TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
  static List<Widget> _widgetOptions;

//  Future<Pokemon> getPokemon(int id) async {
//    Pokemon pokemon = await pokemonBloc.getPokemon(id);
//    return pokemon;
//  }

  @override
  void initState() {
    pokemonBloc.getPokemonList();
    _widgetOptions = <Widget>[
//      ListView.builder(
//        itemBuilder: (context, position) {
//          return FutureBuilder<Pokemon>(
//            future: getPokemon(position),
//            builder: (context, pokemonSnap) {
//              if (!pokemonSnap.hasData) return Container();
//              return ListTile(
//                leading: Image.network(
//                  'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/$position.png',
////            width: 64,
//                  fit: BoxFit.cover,
//                ),
//                title: Text(
//                  '${pokemonSnap.data.name.capitalize()}',
//                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.w400),
//                ),
//                subtitle: Text('#${pokemonSnap.data.id}'),
//              );
//            },
//          );
//        },
//        itemCount: 800,
//      ),
      StreamBuilder<List<Pokemon>>(
        stream: pokemonBloc.pokemons,
        builder: (context, snapshot) {
          if (!snapshot.hasData)
            return SvgPicture.asset(
              'images/logo.svg',
              color: Colors.blueAccent,
            );

          return ListView.builder(
            itemBuilder: (context, position) {
              return ListTile(
                onTap: () => print(''),
                leading: Image.network(
                  'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${position +
                      1}.png',
//            width: 64,
                  fit: BoxFit.cover,
                ),
                title: Text(
                  '${snapshot.data[position].name.capitalize()}',
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.w400),
                ),
                subtitle: Text('#${position + 1}'),
              );
            },
            itemCount: snapshot.data.length,
          );
        },
      ),
      SvgPicture.asset(
        'images/logo.svg',
        color: Colors.cyanAccent,
      ),
      SvgPicture.asset(
        'images/logo.svg',
        color: Colors.greenAccent,
      ),
    ];
  }

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        centerTitle: true,
        title: Text(
          _title,
          style: TextStyle(color: Colors.black),
        ),
        backgroundColor: Colors.transparent,
        bottom: PreferredSize(
          child: Container(
            padding: EdgeInsets.only(left: 8, right: 8),
            margin: EdgeInsets.only(bottom: 16),
//            color: Color.fromARGB(80, 250, 250, 250),
            height: 40,
            width: MediaQuery.of(context).size.width * 0.9,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(18)),
              color: Colors.black.withAlpha(12),
            ),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Icon(Icons.search, color: Colors.black54),
                Expanded(
                    child: TextField(
//                  style: TextStyle(fontSize: 20),
                      decoration: InputDecoration(
                          border: InputBorder.none, hintText: 'Search'),
                    )),
                Icon(Icons.mic, color: Colors.black54)
              ],
            ),
          ),
          preferredSize: Size.fromHeight(56),
        ),
        flexibleSpace: SizedBox(
//          height: 100,
          child: Stack(children: <Widget>[
            _getGradient(),
            Container(
              color: Colors.white.withAlpha(80),
            ),
          ]),
        ),
      ),
      body: Column(
        children: <Widget>[
          _getSeparator(),
          Expanded(
            child: Center(
              child: _widgetOptions.elementAt(_selectedIndex),
            ),
          ),
          _getSeparator()
        ],
      ),
      bottomNavigationBar: SizedBox(
        height: 100,
        child: Stack(children: <Widget>[
          _getGradient(),
          Container(
            color: Colors.white.withAlpha(80),
          ),
          Center(
            child: BottomNavigationBar(
              backgroundColor: Colors.white.withAlpha(0),
              elevation: 0,
              items: <BottomNavigationBarItem>[
                BottomNavigationBarItem(
                  icon: (_selectedIndex == 0)
                      ? SvgPicture.asset(
                    'images/pokemon_icon.svg',
                    semanticsLabel: 'Pokemon',
                    color: Colors.black,
                    width: 30,
                  )
                      : SvgPicture.asset(
                    'images/pokemon_icon.svg',
                    semanticsLabel: 'Pokemon',
                    color: Colors.black.withAlpha(30),
                    width: 26,
                  ),
                  title: Text('Pokemon'),
                ),
                BottomNavigationBarItem(
                  icon: (_selectedIndex == 1)
                      ? SvgPicture.asset(
                    'images/moves_icon.svg',
                    semanticsLabel: 'Pokemon',
                    color: Colors.black,
                    width: 30,
                  )
                      : SvgPicture.asset(
                    'images/moves_icon.svg',
                    semanticsLabel: 'Pokemon',
                    color: Colors.black.withAlpha(30),
                    width: 26,
                  ),
                  title: Text('Moves'),
                ),
                BottomNavigationBarItem(
                  icon: (_selectedIndex == 2)
                      ? SvgPicture.asset(
                    'images/items_icon.svg',
                    semanticsLabel: 'Pokemon',
                    color: Colors.black,
                    width: 30,
                  )
                      : SvgPicture.asset(
                    'images/items_icon.svg',
                    semanticsLabel: 'Pokemon',
                    color: Colors.black.withAlpha(30),
                    width: 26,
                  ),
                  title: Text('Items'),
                ),
              ],
              currentIndex: _selectedIndex,
              selectedItemColor: Colors.black87,
              unselectedItemColor: Colors.black.withAlpha(30),
              onTap: _onItemTapped,
            ),
          ),
        ]),
      ),
    );
  }

  Widget _getSeparator() {
    return Container(
      height: 5,
      decoration: BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.centerLeft,
              end: Alignment.centerRight,
              colors: [
                Color.fromARGB(100, 110, 149, 253),
                Color.fromARGB(100, 111, 222, 250),
                Color.fromARGB(100, 141, 224, 97),
                Color.fromARGB(100, 81, 232, 94),
              ])),
    );
  }

  Widget _getGradient() {
    return Container(
      decoration: BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.centerLeft,
              end: Alignment.centerRight,
              colors: [
                Color.fromARGB(100, 110, 149, 253),
                Color.fromARGB(100, 111, 222, 250),
                Color.fromARGB(100, 141, 224, 97),
                Color.fromARGB(100, 81, 232, 94),
              ])),
    );
  }
}

extension StringExtension on String {
  String capitalize() {
    return "${this[0].toUpperCase()}${this.substring(1)}";
  }
}
