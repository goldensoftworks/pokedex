import 'dart:convert';

import 'package:Pokedex/clients/BasePokemonClient.dart';
import 'package:Pokedex/models/Pokemon.dart';
import 'package:http/http.dart';
import 'package:http/http.dart' as http;

class PokemonClient extends BasePokemonClient {
  /// Pokemon client constructor
  PokemonClient(Client httpClient) : super(httpClient: httpClient);

  Future<List<Pokemon>> getPokemon() async {
    final pokemonUrl = Uri.encodeFull("$baseUrl/pokemon/?offset=0&limit=807");

    try {
      final response = await http.get(pokemonUrl);

      if (response.statusCode == 200) {
        final pokemonJson = jsonDecode(response.body)['results'];

        return (pokemonJson as List).map((e) => Pokemon.fromJson(e)).toList();
      }
    } finally {
      this.httpClient.close();
    }
  }
}
