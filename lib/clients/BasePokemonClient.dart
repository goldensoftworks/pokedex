import 'package:flutter/cupertino.dart';
import 'package:http/http.dart';

abstract class BasePokemonClient {
  /// Base Url or Uri that connect to the web service.
  final String baseUrl = "https://pokeapi.co/api/v2";

  final Client httpClient;

  /// Base constructor class.
  BasePokemonClient({@required this.httpClient});
}
