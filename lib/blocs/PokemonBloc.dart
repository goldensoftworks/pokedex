import 'package:Pokedex/models/Pokemon.dart';
import 'package:Pokedex/repositories/PokemonRepository.dart';
import 'package:flutter/foundation.dart';
import 'package:rxdart/rxdart.dart';

class PokemonBloc {
  final _pokemonRepository = PokemonRepository();

  final _pokemons = BehaviorSubject<List<Pokemon>>();

  Observable<List<Pokemon>> get pokemons => _pokemons.stream;

  getPokemonList() async {
//    List<Pokemon> pokemonList = List();

//    for (var i = 1; i < 80; i++) {
//      final pokemon = await _pokemonRepository.getPokemon(i);
//      pokemonList.add(pokemon);
//    }

    final pokemonList = await _pokemonRepository.getPokemon();
    _pokemons.sink.add(pokemonList);
  }

//  Future<Pokemon> getPokemon(int id) async {
//    final pokemon = await _pokemonRepository.getPokemon(id);
//    return pokemon;
//  }

  dispose() {
    _pokemons.close();
  }
}

final pokemonBloc = PokemonBloc();
