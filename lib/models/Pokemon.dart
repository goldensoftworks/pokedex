import 'HeldItem.dart';
import 'Move.dart';
import 'dart:convert';

class Pokemon {
//  final int baseExperience;
//  final int height;
//  final int id;

//  final bool isDefault;
//  final String locationAreaEncounters;
  final String name;

//  final int order;

//  final int weight;

//  final List<Ability> abilities;
//  final List<Form> forms;
//  final List<GameIndice> gameIndices;
//  final List<HeldItem> heldItems;
//  final List<Move> moves;
//  final List<Stat> stats;
//  final List<Type> types;

//  final Specie species;
//  final Sprite sprites;

  Pokemon(
    //      this.baseExperience,
//      this.height,
//      this.id,
//      this.isDefault,
//      this.locationAreaEncounters,
    this.name,
//      this.order,
//      this.weight,
//      this.abilities,
//      this.forms,
//      this.gameIndices,
//      this.heldItems,
//      this.moves,
//      this.stats,
//      this.types,
//      this.species,
//      this.sprites
  );

  Pokemon.fromJson(Map<String, dynamic> json)
      :
//        id = json['id'],
        name = json['name'];

//        order = json['order'];
//        sprites = json['sprites'];

  Map<String, dynamic> toJson() => {
//        'id': id,
        'name': name,
//        'order': order,
      };
}

class Ability {
  final String name;
  final String url;
  final bool isHidden;
  final int slot;

  Ability(this.name, this.url, this.isHidden, this.slot);
}

class Form {
  final String name;
  final String url;

  Form(this.name, this.url);
}

class GameIndice {
  final int gameIndex;
  final String name;
  final String url;

  GameIndice(this.gameIndex, this.name, this.url);
}

class Specie {
  final String name;
  final String url;

  Specie(this.name, this.url);
}

class Sprite {
  final String backDefault;
  final String backFemale;
  final String backShiny;
  final String backShinyFemale;
  final String frontDefault;
  final String frontFemale;
  final String frontShiny;
  final String frontShinyFemale;

  Sprite(
      this.backDefault,
      this.backFemale,
      this.backShiny,
      this.backShinyFemale,
      this.frontDefault,
      this.frontFemale,
      this.frontShiny,
      this.frontShinyFemale);
}

class Stat {
  final int baseStat;
  final int effort;
  final String name;
  final String url;

  Stat(this.baseStat, this.effort, this.name, this.url);
}

class Type {
  final int slot;
  final String name;
  final String url;

  Type(this.slot, this.name, this.url);
}
