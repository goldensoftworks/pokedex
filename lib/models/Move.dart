class Move {
  final String name;
  final String url;

  final List<VersionGroupDetail> versionGroupDetail;

  Move(this.name, this.url, this.versionGroupDetail);
}

class VersionGroupDetail {
  final int levelLearnedAt;
  final String moveLearnMethodName;
  final String moveLearnMethodUrl;
  final String versionGroupName;
  final String versionGroupUrl;

  VersionGroupDetail(this.levelLearnedAt, this.moveLearnMethodName,
      this.moveLearnMethodUrl, this.versionGroupName, this.versionGroupUrl);
}
