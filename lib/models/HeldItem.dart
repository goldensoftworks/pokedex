class HeldItem {
  final String name;
  final String url;
  final int id;
  final bool isDefault;
  final String locationAreaEncounters;

  final List<VersionDetail> versionDetails;

  HeldItem(this.name, this.url, this.id, this.isDefault,
      this.locationAreaEncounters, this.versionDetails);
}

class VersionDetail {
  final int rarity;
  final String name;
  final String url;

  VersionDetail(this.rarity, this.name, this.url);
}
